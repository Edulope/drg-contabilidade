<?php
// Template Name: Home
?>
<?php get_header(); ?>
<section class="main">
    <div class="content">
        <img src="<?php the_field('icone_home'); ?>">
        <h1><?php the_field('titulo_home');?></h1>
        <a href="">Entre em contato</a>
    </div>
    <div class="bg-home"></div>
</section>
<section class="about-us">
    <div class="content">
        <div id="values">
            <div class="value-card">
                <div class="name"><?php the_field('titulo_box_1');?></div>
                <div class="description"><?php the_field('texto_box_1');?></div>
            </div>
            <div class="value-card">
                <div class="name"><?php the_field('titulo_box_2');?></div>
                <div class="description"><?php the_field('texto_box_2');?></div>
            </div>
            <div class="value-card">
                <div class="name"><?php the_field('titulo_box_3');?></div>
                <div class="description"><?php the_field('texto_box_3');?></div>
            </div>
        </div>
        <p><?php the_field('texto_servicos');?>
            <br>
            <?php the_field('texto_empresa');?></p>
    </div>
</section>
<?php get_footer(); ?>