<?php
// Template Name: Serviços
?>

<?php get_header(); ?>
<section class="our-services">
    <div class="content">
        <h2>Nossos Serviços</h2>
        <p>A DGR terceiriza todos os departamentos de modo que a prestação de serviços pode ser desenvolvida nas dependências do cliente, nossa equipe é instalada em função do perfil e necessidades da empresa.</p>
        <div class="services">
            <div class="service-card">
                <p class="category">Legalização de Empresa</p>
                <ul class="list">
                    <li>Abertura de empresas</li>
                    <li>Encerramento definitivo e provisório</li>
                    <li>Inscrição de alvará</li>
                    <li>Inscrição CNPJ</li>
                    <li>Inscrição estadual</li>
                    <li>Alteração contratual</li>
                    <li>Legalização de Food Truck</li>
                </ul>
            </div>
            <div class="service-card">    
                <p class="category">Rotina</p>
                <ul class="list">
                    <li>Abertura de empresas</li>
                    <li>Encerramento definitivo e provisório</li>
                    <li>Inscrição de alvará</li>
                    <li>Inscrição CNPJ</li>
                    <li>Inscrição estadual</li>
                    <li>Alteração contratual</li>
                    <li>Legalização de Food Truck</li>
                </ul>
            </div>
            <div class="service-card">
                <p class="category">Inscrição e Alvarás</p>
                <ul class="list">
                    <li>Abertura de empresas</li>
                    <li>Encerramento definitivo e provisório</li>
                    <li>Inscrição de alvará</li>
                    <li>Inscrição CNPJ</li>
                    <li>Inscrição estadual</li>
                    <li>Alteração contratual</li>
                    <li>Legalização de Food Truck</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>