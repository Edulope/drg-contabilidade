﻿<?php
$contato = get_page_by_title('Contato');
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DGR Contabilidade</title>

    <!-- Fontes e Arquivos CSS -->
    <link href="https://fonts.googleapis.com/css?family=Aldrich&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet" href="style.css">

    <!-- Dizer que � um Header -->
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="container">
            <a href="/DGRcontabilidade/home"><p class="logo">DGR</p></a>
            <nav>
                <a href="/DGRcontabilidade/servicos">Serviços</a>
                <a href="/DGRcontabilidade/noticias/">Noticias</a>
                <a href="/DGRcontabilidade/cont">Contato</a>
                <div class="telephone">
                    <p class="tel-number"><?php the_field('telefones', $contato);?></p>
                </div>
            </nav>
        </div>
    </header>