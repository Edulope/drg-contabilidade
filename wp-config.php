<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'DGRcontabilidade' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'admin' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'admin' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>7PE}0A8ULU%F1x!Pat(1Gh[sQ8Aa}jxY6iuBQyL`E9n[FgThNO>S+5#KY1^W{H4' );
define( 'SECURE_AUTH_KEY',  '~YB} +Sx>IU>=)cx@fivK:Ab1$/IL]S[&*]Tq2wd^}JdJT#_zW:8 G)Uy`H#$,6u' );
define( 'LOGGED_IN_KEY',    ']IvYmrpucsDGiI~gfsL~eK{^tj8wzs& : O. PdUbr#LJ;+Gv|J[b!dP++c?ZU(f' );
define( 'NONCE_KEY',        '1,_z#v8-;tI#]bB:-vluMU6?drTlnv:7nI-.7&ksGLTdRA/Y7w}1*&qi spPVWD|' );
define( 'AUTH_SALT',        'sArd,mwo5A }VdM!Y0aT_{,g:T1?JzZCi~#[uj>I!p/l`~;Z3|bBZ28AFbzyl[hj' );
define( 'SECURE_AUTH_SALT', '&i&w~IM[ T9FLdNMsPb-?vu_)]P/>aS50/@)V3EoOp~et,PXteF30CCl~K96zOV@' );
define( 'LOGGED_IN_SALT',   'pV fMKd/FrYiKm{nzOHB8uf9QyTe~Box&2%MK7m8v_Tfe(<I)s yYU%guEqpKMZ>' );
define( 'NONCE_SALT',       'QX)Xl?2SnpA...M%n;t Wr%%2_> -grP*KiX8wp|zSC)o<yFYc@:5KO6a)2:@V=~' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
